# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, UserError

class Project(models.Model):
	_inherit = 'project.project'
	_description = 'Project'

	po_date = fields.Date(string="PO Date")
	date = fields.Date(string="Plan End Date")
	date_start = fields.Date(string="Plan Start Date")
	vertical_ids = fields.Many2many('project.verticals',string="Verticals")
	technology_ids = fields.Many2many('project.technology',string="Technology",domain="[('vertical_ids', '=', vertical_ids)]")
	status = fields.Selection([('draft', 'Draft'),('account', 'Account'),('wip', 'Work in Progress'), ('closed', 'Closed')], string='Status',default='draft')
	remarks = fields.Text(string="Remarks")
	count = fields.Integer(string="Count",default=1)
	project_value = fields.Float(string="Project value")
	user_id = fields.Many2one('res.users',string='Project Managers', track_visibility="onchange")

	proj_date = fields.Date(string="Project Date",invisible=True)
	seq_id = fields.Char(string="P ID")
	proj_priority = fields.Selection([('high', 'High'), ('medium', 'Medium'), ('low', 'Low')], string='Project Priority')
	clint_priority = fields.Selection([('high', 'High'), ('medium', 'Medium'), ('low', 'Low')], string='Client Priority')
	proj_user_id = fields.Many2many('hr.employee',string="Project Users")
	description = fields.Text(string="Description")
	mobile = fields.Char(string="Mobile")
	mail = fields.Char(string="Mail")
	url = fields.Text(string="Repository URL")
	design_link = fields.Text(string="Design link")
	dev_url = fields.Text(string="Dev Server URL")
	staging_url = fields.Text(string="Staging URL")
	live_url = fields.Text(string="Live URL")
	mobile = fields.Char(string="Customer Mobile")
	mail = fields.Char(string="Customer Mail")
	# fields for Escalation tab
	escalation = fields.Text(string="Escalation")

	manager_user = fields.Boolean(string="Manager user",compute='_compute_manager_user')
	current_user = fields.Many2one('res.users','Current User', default=lambda self: self.env.user)

	@api.multi
	def send_to_account(self):
		self.write({'status': 'account'})

	@api.multi
	def send_to_pm(self):
		if not self.partner_id:
			raise UserError(_('Please add "Customer"'))
		self.write({'status': 'wip'})

	@api.multi
	def button_closed(self):
		self.write({'status': 'closed'})

	@api.multi
	@api.depends('current_user')
	def _compute_manager_user(self):
		is_manager = self.env.user.has_group('project.group_project_manager')
		is_access_rights = self.env.user.has_group('base.group_erp_manager')
		is_settings = self.env.user.has_group('base.group_system')
		if is_manager:
			if not is_access_rights:
				if not is_settings:
					self.manager_user = True

	# Onchange
	@api.onchange('partner_id')
	def onchange_partner_id(self):
		if self.partner_id:
			self.mobile = self.partner_id.mobile
			self.mail = self.partner_id.email

	# Sequence Generation
	@api.model
	def create(self, vals):
		vals['seq_id'] = self.env['ir.sequence'].next_by_code('project.project.seq') or '/'
		return super(Project, self).create(vals)

	# Unlink
	@api.multi
	def unlink(self):
		if self.filtered(lambda x:x.status not in 'draft'):
			raise UserError(_('You cannot delete the record which is not in draft !..'))
		return super(Project, self).unlink()
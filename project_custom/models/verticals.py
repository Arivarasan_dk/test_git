# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ProjectVerticals(models.Model):
	_name = 'project.verticals'
	_description = 'Verticals'

	name = fields.Char(string="Name")
	code = fields.Char(string="Code")

	# @api.multi
	# def name_get(self):
	# 	result = []
	# 	for line in self:
	# 		name = str(line.code) + ' - ' + line.name
	# 		result.append((line.id, name))
	# 	return result
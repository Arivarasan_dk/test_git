# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ProjectTechnology(models.Model):
	_name = 'project.technology'
	_description = 'Technology'

	name = fields.Char(string="Name")
	code = fields.Char(string="Code")
	vertical_ids = fields.Many2one('project.verticals',string="Verticals")

	# @api.multi
	# def name_get(self):
	# 	result = []
	# 	for line in self:
	# 		name = str(line.code) + ' - ' + line.name
	# 		result.append((line.id, name))
	# 	return result
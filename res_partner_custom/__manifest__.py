# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Res partner',
    'version' : '1.1',
	'author' : 'Indglobal digital private limited',
    'summary': 'Adding additional fields in res partner',
    'sequence': 1,
    'description': """Inheriting in res partner""",
    'category' : 'Contacts',
    'website': 'https://www.indglobal.com',
    'depends' : ['base'],
    'data': ['views/res_partner.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'OEEL-1',
}

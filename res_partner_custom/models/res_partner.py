# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models,_
import re
from odoo.exceptions import ValidationError,UserError

class ResPartnerInherit(models.Model):
	_inherit = "res.partner"

	@api.one
	@api.constrains('vat','phone','email')
	def _check_vat(self):
		if self.vat:
			if(len(self.vat)!=15):
				raise ValidationError(_('GSTIN Number length should be in 15 !..'))
		if self.phone:
			match = re.match("^[0-9]*$",self.phone)
			if match == None:
				raise ValidationError(_("Enter correct 'Phone Number'"))
		if self.email:
			match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
			if match == None:
				raise ValidationError('Not a valid E-mail ID') 
		return True